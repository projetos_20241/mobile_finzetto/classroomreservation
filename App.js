import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./src/loginScreen";
import HomePage from "./src/homePage";
import SignupScreen from "./src/signupScreen";
import Cam from "./src/cam/camera";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: "Login" }}
        />
        <Stack.Screen
          name="HomePage"
          component={HomePage}
          options={{ title: "Classroom Reservation" }}
        />
        <Stack.Screen
          name="SignupScreen"
          component={SignupScreen}
          options={{ title: "Cadastre-se" }}
        />
        <Stack.Screen 
        name="Cam" 
        component={Cam} 
        options={{ title: "FOTO" }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
