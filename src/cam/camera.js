import { Camera, CameraType } from "expo-camera";
import { useRef, useState } from "react";
import {
  Button,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Modal,
  Image,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

export default function Cam() {
  const [type, setType] = useState(CameraType.back);
  const [permission, requestPermission] = Camera.useCameraPermissions();
  const camRef = useRef(null);
  const [capturedPhoto, setCapturedPhoto] = useState(null);
  const [open, setOpen] = useState(false);
  if (!permission) {
    // Camera permissions are still loading
    return <View />;
  }

  if (!permission.granted) {
    // Camera permissions are not granted yet
    return (
      <View style={styles.container}>
        <Text style={{ textAlign: "center" }}>
          Precisamos da sua permissão para mostrar a câmera
        </Text>
        <Button onPress={requestPermission} title="Permitir" />
      </View>
    );
  }

  function toggleCameraType() {
    setType((current) =>
      current === CameraType.back ? CameraType.front : CameraType.back
    );
  }

  async function takePicture() {
    if (camRef) {
      const data = await camRef.current.takePictureAsync();
      setCapturedPhoto(data.uri);
      setOpen(true);
      console.log(data);
    }
  }

  return (
    <View style={styles.container}>
      <Camera style={styles.camera} type={type} ref={camRef}>
        <TouchableOpacity
          style={styles.buttonReverse}
          onPress={toggleCameraType}
        >
          <MaterialIcons name="flip-camera-ios" size={60} color="white" />
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttonTake} onPress={takePicture}>
          <MaterialIcons name="camera" size={60} color="white" />
        </TouchableOpacity>
      </Camera>
      {capturedPhoto && (
        <Modal
          animationType="slide"
          transparent={true}
          visible={open} 
        >
          <View style={styles.modal}>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => {
              setOpen(false);
            }}
          >
            <MaterialIcons name="close" size={40} color="red" />
          </TouchableOpacity>
          
            <Image source={{ uri: capturedPhoto }} style={styles.photo} />
          </View>
        </Modal>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  camera: {
    flex: 1,
  },
  buttonReverse: {
    position: "absolute",
    bottom: 50,
    right: 30,
    backgroundColor: "transparent",
  },
  buttonTake: {
    position: "absolute",
    bottom: 50,
    left: 30,
    backgroundColor: "transparent",
  },
  text: {
    fontSize: 24,
    fontWeight: "bold",
    color: "white",
  },
  modal: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end",
    margin: 20,
  },
  closeButton: {
    position: "absolute",
    top: 100,
    left: 5,
    margin: 10,
  },
  photo: {
    width: "100%",
    height: 400,
  },
});